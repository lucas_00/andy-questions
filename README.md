# andy-questions

## Description
Cuestionario dinamico con Vue JS.

## Preview
Click [aqui](https://confident-jang-bf579e.netlify.app/) para preview.

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
